import java.io.*;
import java.net.*;
import java.util.*;



public class Pop3 {

	boolean debug = false;				// set this to true to print out debug information
	String _server;						// name of the pop3-server
	String _port;						// the port on the server pop3 runs on (normaly 110)
	String _username;					// username/login-name
	String _password;					// password
	DataOutputStream outToServer;		// the bytes we are going to send to the server
	BufferedReader inFromServer;		// the bytes we are going to read from the server
	int noMessages;						// the total number of messages/emails on the server

	// supports max 100 messages
	String sender[] = new String[100];	// the sender of the email
	String title[] = new String[100];	// the title / subject of the email
	String bytes[] = new String[100];	// how many bytes the email is
	String text[] = new String[100];	// the text of the email


	Pop3(String server, String port, String username, String password){
		_server = server;
		_port = port;
		_username = username;
		_password = password;
		
		getMailFromServer();
		listMail();		
	}



	// open connection and get the messages/emails from the server
	void getMailFromServer(){
		try{

			String receiveStr;
			String numberMessages;
			int i;


			debug("Receiving emails from '"+_server+":"+_port+" as user '"+_username+"':");

			Socket socket = new Socket(_server, Integer.parseInt(_port));
			
			outToServer = new DataOutputStream(socket.getOutputStream());
			inFromServer = new BufferedReader( new InputStreamReader(socket.getInputStream()) );

			receiveStr = receive();
			if ( receiveStr.startsWith("-ERR") ){
				print("Connetion problems: "+receiveStr);
				return;
			}

			send("user "+_username);
			receiveStr = receive();
			if ( receiveStr.startsWith("-ERR") ){
				print("Username not accepted: "+receiveStr);
				return;
			}

			send("pass "+_password);
			receiveStr = receive();
			if ( receiveStr.startsWith("-ERR") ){
				print("Password not accepted: "+receiveStr);
				return;
			}

			send("list");
			receiveStr = receive();
			if ( receiveStr.startsWith("-ERR") ){
				print("Unable to receive messages: "+receiveStr);
				return;
			}

			receiveStr = receive();
			i = 0;
			while( !receiveStr.equals(".") ){
				i++;
				bytes[i] = parse(receiveStr," ")[1];
				receiveStr = receive();
			}

			noMessages = i;

			for (i=1; i<=noMessages; i++){
				send("retr "+i);
				receiveMultiple(i);
			}
			
			send("quit");
			
			socket.close();

		} catch (UnknownHostException e){
			print("Unkown host: "+_server);
		} catch (Exception e) {
			e.printStackTrace();	
		}

	}




	// send a command to the server
    void send (String cmdline) {
		try {
			debug(">>" + cmdline);
			outToServer.writeBytes(cmdline + "\r\n");
		} catch (Exception e) {
			e.printStackTrace();	
		}
    }




	// receive a line from server
    String receive() {
		try {
			String line = "";
			line = inFromServer.readLine();
			debug("<<" + line);

			if (line == null) 	{
		    	debug("The line is empty");
		    	line = "-ERR emptyLine";
			}
			
			if (line.trim().startsWith("-ERR")) {
				debug("Server responded with an error!");
	    		return line;
			} else {
				return line;
			}

		} catch (Exception e) {
			e.printStackTrace();	
		}
		return null;
    }




	// receive multiple lines
    void receiveMultiple(int currentMsgNo) {
		try {
			String line = "";
		    boolean finished = false;
		    int lineNo = 0;
		    boolean body = false;
		    boolean skipLine = false;

		    while (!finished) {
		    	skipLine = false;					// reset every round
				line = inFromServer.readLine();
				lineNo++;
				debug("<<" + line.length() + " '" + line +"'");

				// first line of message
				if (lineNo == 1) {
		    
		    		if (line.trim().startsWith("-ERR ")) {
						debug("Server responded with an error!");
						finished = true;
		    		} else if (line.trim().startsWith("+OK")) {
			    		//Everything looks OK
			    		//debug(line);
					} else {
			    		debug("Received strange response:");
			    		debug("'" + line + "'");
			    		finished = true;
					}

				// the rest of the messages
				} else {

					if (line.startsWith(".")) {
						if (line.length() == 1){
							finished = true;
			    			text[currentMsgNo] = text[currentMsgNo].concat(".");		// ADDED TO SATISFY 9331 TESTCASES
						} else {
							// if line starts with '.' another '.' is automatically added, so don't save the first one
							text[currentMsgNo] = text[currentMsgNo].concat("\n"+line.substring(1));
						}
		    		} else {

			    		if (line.trim().startsWith("From: ")){
			    			if ( line.indexOf("<")>-1){
				    			sender[currentMsgNo] = parse(line,"<")[1];
				    			sender[currentMsgNo] = sender[currentMsgNo].replace('>',' ');
				    		} else {
				    			sender[currentMsgNo] = parse(line," ")[1];
				    		}
			    			sender[currentMsgNo] = sender[currentMsgNo].trim();
			    			debug(sender[currentMsgNo]);
			    		} else if (line.trim().startsWith("Subject: ")){
			    			title[currentMsgNo] = line.replaceAll("Subject: ","");
							debug(title[currentMsgNo]);
			    		} else if (line.length()==0 && !body){
			    			debug("body == true");
			    			body = true;
			    			skipLine = true;
			    		}

			    		if (body && !skipLine){
			    			if (text[currentMsgNo]==null){
					    		text[currentMsgNo]=line;
			    			} else {
			    				text[currentMsgNo]=text[currentMsgNo].concat("\n"+line);
			    			}
							//debug(text[currentMsgNo]);
			    		}

					}
				}
		
			}   // end of while(!finished)

		} catch (Exception e) {
			e.printStackTrace();	
		}
		return;
    }




    String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }

		return tokens;
    }




	void listMail(){
		int i;
		
		for (i=1;i<=noMessages;i++){
			System.out.println(sender[i]+" "+title[i]+" "+bytes[i]+"\n"+text[i]);
		}
	}


	void debug (String line){
		if (debug)
			System.out.println("DEBUG: "+line);
	}



	void print (String line){
			System.out.println(line);
	}


}

